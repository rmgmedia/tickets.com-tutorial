<?php

interface ServiceLocatorInterface
{
    /**
     * @param string $serviceAlias
     *
     * @return object
     */
    public function create($serviceAlias);

    /**
     * @param string $serviceAlias
     *
     * @return object
     */
    public function get($serviceAlias);

    /**
     * @param string $serviceAlias
     * @param object$service
     *
     * @return object
     */
    public function set($serviceAlias, $service);

    /**
     * @param string $serviceAlias
     *
     * @return bool
     */
    public function has($serviceAlias);
}
