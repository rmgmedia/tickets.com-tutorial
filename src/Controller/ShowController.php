<?php

namespace Controller;

use App;
use TemplateRenderer;

class ShowController
{
    /**
     * @return string
     * @throws \Exception
     */
    public function indexAction()
    {
        /** @var TemplateRenderer $templateRenderer */
        $templateRenderer = App::getServiceLocator()->get('TemplateRenderer');
        return $templateRenderer->render('show/index');
    }

    /**
     * @param array $data
     *
     * @return string
     * @throws \Exception
     */
    public function viewAction(array $data)
    {
        /** @var TemplateRenderer $templateRenderer */
        $templateRenderer = App::getServiceLocator()->get('TemplateRenderer');
        return $templateRenderer->render('show/view', $data);
    }
}