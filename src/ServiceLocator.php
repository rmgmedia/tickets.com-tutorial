<?php

use Factory\FactoryInterface;

class ServiceLocator implements ServiceLocatorInterface
{
    /** @var array */
    protected $config;

    /** @var array */
    protected $services;

    /**
     * ServiceLocator constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $serviceAlias
     *
     * @return object
     * @throws Exception
     */
    public function create($serviceAlias)
    {
        if ($this->canCreateService($serviceAlias)) {
            if (!empty($this->config['invokables'][$serviceAlias])) {
                $className = $this->config['invokables'][$serviceAlias];
                return new $className;
            }

            if (!empty($this->config['factories'][$serviceAlias])) {
                /** @var FactoryInterface $factory */
                $factoryName = $this->config['factories'][$serviceAlias];
                $factory = new $factoryName;
                return $factory->create($this);
            }
        }
        throw new Exception("Service locator was unable to create class from alias \"$serviceAlias\".");
    }

    /**
     * @param string $serviceAlias
     *
     * @return object
     * @throws Exception
     */
    public function get($serviceAlias)
    {
        if (!isset($this->services[$serviceAlias])) {
            $this->services[$serviceAlias] = $this->create($serviceAlias);
        }

        return $this->services[$serviceAlias];
    }



    /**
     * @param string $serviceAlias
     * @param object $service
     */
    public function set($serviceAlias, $service)
    {
        $this->services[$serviceAlias] = $serviceAlias;
    }

    /**
     * @param string $serviceAlias
     * @return bool
     */
    public function has($serviceAlias)
    {
        return isset($this->services[$serviceAlias]) || $this->canCreateService($serviceAlias);
    }

    /**
     * @param $serviceAlias
     *
     * @return bool
     */
    protected function canCreateService($serviceAlias)
    {
        return isset($this->config['invokables'][$serviceAlias]) || isset($this->config['factories'][$serviceAlias]);
    }
}