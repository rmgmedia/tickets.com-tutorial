<?php

namespace Factory\Mapper;

use Mapper\UrlAliasMapper;
use Factory\FactoryInterface;
use PDO;
use ServiceLocatorInterface;

class UrlAliasMapperFactory implements FactoryInterface
{

    public function create(ServiceLocatorInterface $serviceLocator)
    {
        /** @var PDO $dbAdapter */
        $dbAdapter = $serviceLocator->get('DatabaseAdapter');
        return new UrlAliasMapper($dbAdapter);
    }
}