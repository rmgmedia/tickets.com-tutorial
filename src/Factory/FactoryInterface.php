<?php

namespace Factory;

use ServiceLocatorInterface;

interface FactoryInterface
{
    public function create(ServiceLocatorInterface $serviceLocator);
}