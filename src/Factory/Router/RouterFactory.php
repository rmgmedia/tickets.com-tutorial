<?php

namespace Factory\Router;

use Factory\FactoryInterface;
use ServiceLocatorInterface;
use Router\Router;
use UrlAliasMapper;

class RouterFactory implements FactoryInterface
{
    public function create(ServiceLocatorInterface $serviceLocator)
    {
        /** @var UrlAliasMapper $urlAliasMapper */
        $urlAliasMapper = $serviceLocator->get('UrlAliasMapper');
        $router = new Router($serviceLocator, $urlAliasMapper);
        return $router;
    }
}