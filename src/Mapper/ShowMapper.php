<?php

namespace Mapper;

use DateTime;
use PDO;

class ShowMapper
{
    /** @var PDO */
    protected $db;

    /**
     * ShowMapper constructor.
     * @param PDO $dbAdapter
     */
    public function __construct(PDO $dbAdapter)
    {
        $this->db = $dbAdapter;
    }

    /**
     * @return array
     */
    public function getPopularShows()
    {
        $stmt = $this->db->query("SELECT * FROM `show` ORDER BY `popularity` DESC LIMIT 5");
        $shows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $shows = array_map(function($show) {
            $show['date'] = new DateTime($show['date']);
            return $show;
        }, $shows);
        return $shows;
    }

    /**
     * @return array
     */
    public function getAllShows()
    {
        $stmt = $this->db->query("SELECT * FROM `show` ORDER BY `date` DESC");
        $shows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $shows = array_map(function($show) {
            $show['date'] = new DateTime($show['date']);
            return $show;
        }, $shows);
        return $shows;
    }

    /**
     * @param int $id
     *
     * @return array|bool
     */
    public function getShow($id)
    {
        $stmt = $this->db->query("SELECT * FROM `show` WHERE `id` = " . $this->db->quote($id));
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($rows) {
            $show = $rows[0];
            $show['date'] = new DateTime($show['date']);
            return $show;
        }
        return false;
    }
}