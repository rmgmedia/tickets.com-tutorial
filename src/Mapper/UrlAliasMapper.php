<?php

namespace Mapper;

use PDO;

class UrlAliasMapper
{
    /** @var PDO */
    protected $db;

    /**
     * UrlAliasMapper constructor.
     * @param PDO $dbAdapter
     */
    public function __construct(PDO $dbAdapter)
    {
        $this->db = $dbAdapter;
    }

    /**
     * @param int $requestUri
     *
     * @return array|bool
     */
    public function getUrlAliasByRequestUri($requestUri)
    {
        $stmt = $this->db->query("SELECT * FROM `url_alias` WHERE `request_uri` = " . $this->db->quote($requestUri));
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($rows) {
            return $rows[0];
        }
        return false;
    }
}