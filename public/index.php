<?php

$options = require('../config/application.php');
require('../src/App.php');
App::init($options);
echo App::run();
