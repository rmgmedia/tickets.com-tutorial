<?php

return [
    'php_ini' => [
        'display_errors' => 'on',
        'error_reporting' => -1,
    ],
    'service_locator' => [
        'invokables' => [
            'TemplateRenderer' => 'TemplateRenderer',
        ],
        'factories' => [
            'DatabaseAdapter' => 'Factory\DatabaseAdapterFactory',
            'Router' => 'Factory\Router\RouterFactory',
            'UrlAliasMapper' => 'Factory\Mapper\UrlAliasMapperFactory',
            'Controller\ArtistController' => 'Factory\Controller\ArtistControllerFactory',
            'Controller\IndexController' => 'Factory\Controller\IndexControllerFactory',
            'Controller\ShowController' => 'Factory\Controller\ShowControllerFactory',
            'Mapper\ArtistMapper' => 'Factory\Mapper\ArtistMapperFactory',
            'Mapper\ShowMapper' => 'Factory\Mapper\ShowMapperFactory',
        ]
    ],
];